﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;

namespace test_ARMO
{
    //this class validates provided criteria and performs the search
    public class FileTextSearcher
    {
        private string _startingDirectory;
        public string StartingDirectory
        {
            get { return _startingDirectory; }
            private set
            {
                if (!String.IsNullOrEmpty(value) && Directory.Exists(value))
                    _startingDirectory = value;
                else throw new System.ArgumentException("Не получилось найти директорию");
            }
        }

        private string _filenamePattern;
        public string FilenamePattern
        {
            get { return _filenamePattern; }
            private set
            {
                if (String.IsNullOrEmpty(value))
                {
                    _filenamePattern = null;
                }
                else
                {
                    //disallow invalid filename chars, except wildcard:
                    var disallowedChars = Path.GetInvalidFileNameChars().Except(new List<char> { '?', '*' }).ToArray();
                    if (value.IndexOfAny(disallowedChars) < 0)
                    {
                        _filenamePattern = value;
                        String convertedMask = "^" + Regex.Escape(value).Replace("\\*", ".*").Replace("\\?", ".") + "$";
                        _pattern = new Regex(convertedMask, RegexOptions.IgnoreCase);
                    }
                    else throw new System.ArgumentException("В имени файла есть недопустимые символы");
                }
            }
        }

        public string TextToFind { get; }
        
        public ObservableCollection<string> Results;

        private Regex _pattern;

        //takes parameters for search and collection for results
        public FileTextSearcher(string startingDirectory, string filenamePattern, string textToFind, ObservableCollection<string> results)
        {
            try
            {
                Results = results;               
                StartingDirectory = startingDirectory;
                FilenamePattern = filenamePattern;
                TextToFind = textToFind;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //performs the search, returns the names of the scanned files
        public IEnumerable<string> Search()
        {
            var files = Directory.EnumerateFiles(StartingDirectory, "*", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                yield return file;
                if (FilenameMatch(file) && TextMatch(file))
                {
                    Results.Add(file);
                }
            }
        }

        private bool FilenameMatch(string file)
        {
            if (String.IsNullOrEmpty(FilenamePattern)) return true;
            var test = _pattern.IsMatch(Path.GetFileNameWithoutExtension(file));
            var test2 = Path.GetFileNameWithoutExtension(file);
            return _pattern.IsMatch(Path.GetFileNameWithoutExtension(file));
        }

        private bool TextMatch(string file)
        {
            if (String.IsNullOrEmpty(TextToFind)) return true;
            return File.ReadLines(file).Any(l => l.Contains(TextToFind));
        }

    }
}
