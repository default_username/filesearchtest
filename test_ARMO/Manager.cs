﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Serialization;

namespace test_ARMO
{
    //this class manages starting search, pausing, saving data to xml, etc
    class Manager : INotifyPropertyChanged
    {
        private DateTime _timeElapsedDT;

        private IEnumerator _enumerator;

        private Timer timer;

        public event PropertyChangedEventHandler PropertyChanged;

        private FileTextSearcher Searcher;

        public ObservableCollection<string> Results;


        private bool Paused;

        private bool Finished;

        #region properties
        private string _startingDirectory;
        public string StartingDirectory
        {
            get { return _startingDirectory; }
            set
            {
                _startingDirectory = value;
                OnPropertyChanged(nameof(StartingDirectory));
            }
        }

        private string _filenamePattern;
        public string FilenamePattern
        {
            get { return _filenamePattern; }
            set
            {
                _filenamePattern = value;
                OnPropertyChanged(nameof(FilenamePattern));
            }
        }

        private string _textToFind;
        public string TextToFind
        {
            get { return _textToFind; }
            set
            {
                _textToFind = value;
                OnPropertyChanged(nameof(TextToFind));
            }
        }

        private string _currentFile;
        public string CurrentFile
        {
            get { return _currentFile; }
            set
            {
                _currentFile = value;
                OnPropertyChanged(nameof(CurrentFile));
            }
        }
               
        private int _filesSearched;
        public int FilesSearched
        {
            get { return _filesSearched; }
            set
            {
                _filesSearched = value;
                OnPropertyChanged("FilesSearched");
            }
        }

        private string _timeElapsed;
        public string TimeElapsed
        {
            get { return _timeElapsed; }
            set
            {
                _timeElapsed = value;
                OnPropertyChanged("TimeElapsed");
            }
        }
        #endregion

        public Manager()
        {
            CurrentFile = "";
            FilesSearched = 0;
            Finished = false;
            Paused = false;            
            timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += OnTimerElapsed;
            LoadEnteredData();
            Results = new ObservableCollection<string>();
        }

        public void StartSearch()
        {
            Paused = false;
            Finished = false;
            FilesSearched = 0;
            _timeElapsedDT = new DateTime();
            
            try
            {   Task.Run(() => SaveEnteredData());
                Searcher = new FileTextSearcher(StartingDirectory, FilenamePattern, TextToFind, Results);
                Results = Searcher.Results;
                _enumerator = Searcher.Search().GetEnumerator();
                Task.Run(() => timer.Start()); //no idea why it doesn't throw when updating UI
                OnTimerElapsed(new object(), new EventArgs());
                Search();
            }
            catch (Exception ex)
            {
                throw ex;
            }                    
        }

        private void Search()
        {
            while(!Paused && _enumerator.MoveNext())
            {
                CurrentFile = (string)_enumerator.Current;
                FilesSearched++;
            }
            if(!_enumerator.MoveNext())
            {
                Finished = true;
                timer.Stop();
            }
        }

        public void Pause()
        {
            Paused = true;
            timer.Stop();
        }

        public void Resume()
        {
            Paused = false;
            timer.Start();
            Search();
        }

        //writes to xml in localAppData
        private void SaveEnteredData()
        {
            try
            {
                var toSave = new DataToSave
                {
                    StartingDirectory = this.StartingDirectory,
                    FilenamePattern = this.FilenamePattern,
                    TextToFind = this.TextToFind
                };
                XmlSerializer xs = new XmlSerializer(typeof(DataToSave));
                TextWriter tw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\FileSearchParameters.xml");
                xs.Serialize(tw, toSave);
                tw.Close();
            }
            catch (Exception ex)
            {
                throw new IOException("Не получилось сохранить данные");
            }
        }

        //retrieves from xml
        private void LoadEnteredData()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\FileSearchParameters.xml";
            if (!File.Exists(path)) return;

            XmlSerializer xs = new XmlSerializer(typeof(DataToSave));
            using (var sr = new StreamReader(path))
            {
                var DataToLoad = (DataToSave)xs.Deserialize(sr);
                this.StartingDirectory = DataToLoad.StartingDirectory;
                this.FilenamePattern = DataToLoad.FilenamePattern;
                this.TextToFind = DataToLoad.TextToFind;
            }
        }
        
        private void OnPropertyChanged(string propertyName = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch  (Exception ex)
            {
                //pretend everything is on the right thread
            }
        }

        private void OnTimerElapsed(object sender, EventArgs e)
        {
            _timeElapsedDT = _timeElapsedDT.AddSeconds(1.0);
            TimeElapsed = _timeElapsedDT.ToString("HH:mm:ss");
        }

    }

    public struct DataToSave
    {
        public string StartingDirectory;
        public string FilenamePattern;
        public string TextToFind;
    }
}
