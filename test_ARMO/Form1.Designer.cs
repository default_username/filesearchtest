﻿namespace test_ARMO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.DirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FileNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.StringToFindTextBox = new System.Windows.Forms.TextBox();
            this.StartSearchButton = new System.Windows.Forms.Button();
            this.PauseButton = new System.Windows.Forms.Button();
            this.ResumeButton = new System.Windows.Forms.Button();
            this.FoundFilesLabel = new System.Windows.Forms.Label();
            this.NowScanningLabel = new System.Windows.Forms.Label();
            this.CurrentFileLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FileCountLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.ResultsTreeView = new System.Windows.Forms.TreeView();
            this.managerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.managerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Стартовая директория:";
            // 
            // DirectoryTextBox
            // 
            this.DirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectoryTextBox.Location = new System.Drawing.Point(151, 16);
            this.DirectoryTextBox.Name = "DirectoryTextBox";
            this.DirectoryTextBox.Size = new System.Drawing.Size(566, 20);
            this.DirectoryTextBox.TabIndex = 1;
            this.DirectoryTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Искать в имени файла:\r\n";
            // 
            // FileNameTextBox
            // 
            this.FileNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileNameTextBox.Location = new System.Drawing.Point(151, 46);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.Size = new System.Drawing.Size(566, 20);
            this.FileNameTextBox.TabIndex = 3;
            this.FileNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Искать внутри файлов: ";
            // 
            // StringToFindTextBox
            // 
            this.StringToFindTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StringToFindTextBox.Location = new System.Drawing.Point(151, 73);
            this.StringToFindTextBox.Name = "StringToFindTextBox";
            this.StringToFindTextBox.Size = new System.Drawing.Size(566, 20);
            this.StringToFindTextBox.TabIndex = 5;
            this.StringToFindTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // StartSearchButton
            // 
            this.StartSearchButton.AutoSize = true;
            this.StartSearchButton.Location = new System.Drawing.Point(12, 109);
            this.StartSearchButton.Name = "StartSearchButton";
            this.StartSearchButton.Size = new System.Drawing.Size(86, 23);
            this.StartSearchButton.TabIndex = 6;
            this.StartSearchButton.Text = "Начать поиск";
            this.StartSearchButton.UseVisualStyleBackColor = true;
            this.StartSearchButton.Click += new System.EventHandler(this.StartSearchButton_Click);
            // 
            // PauseButton
            // 
            this.PauseButton.AutoSize = true;
            this.PauseButton.Location = new System.Drawing.Point(104, 109);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Size = new System.Drawing.Size(75, 23);
            this.PauseButton.TabIndex = 7;
            this.PauseButton.Text = "Пауза";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // ResumeButton
            // 
            this.ResumeButton.AutoSize = true;
            this.ResumeButton.Location = new System.Drawing.Point(185, 109);
            this.ResumeButton.Name = "ResumeButton";
            this.ResumeButton.Size = new System.Drawing.Size(113, 23);
            this.ResumeButton.TabIndex = 8;
            this.ResumeButton.Text = "Продолжить поиск";
            this.ResumeButton.UseVisualStyleBackColor = true;
            this.ResumeButton.Click += new System.EventHandler(this.ResumeButton_Click);
            // 
            // FoundFilesLabel
            // 
            this.FoundFilesLabel.AutoSize = true;
            this.FoundFilesLabel.Location = new System.Drawing.Point(12, 159);
            this.FoundFilesLabel.Name = "FoundFilesLabel";
            this.FoundFilesLabel.Size = new System.Drawing.Size(105, 13);
            this.FoundFilesLabel.TabIndex = 9;
            this.FoundFilesLabel.Text = "Найденные файлы:";
            // 
            // NowScanningLabel
            // 
            this.NowScanningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NowScanningLabel.AutoSize = true;
            this.NowScanningLabel.Location = new System.Drawing.Point(357, 178);
            this.NowScanningLabel.Name = "NowScanningLabel";
            this.NowScanningLabel.Size = new System.Drawing.Size(133, 13);
            this.NowScanningLabel.TabIndex = 11;
            this.NowScanningLabel.Text = "Сейчас обрабатывается:";
            // 
            // CurrentFileLabel
            // 
            this.CurrentFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CurrentFileLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.CurrentFileLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CurrentFileLabel.Location = new System.Drawing.Point(496, 176);
            this.CurrentFileLabel.Name = "CurrentFileLabel";
            this.CurrentFileLabel.Size = new System.Drawing.Size(221, 15);
            this.CurrentFileLabel.TabIndex = 12;
            this.CurrentFileLabel.Text = " ";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(357, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Обработано файлов:";
            // 
            // FileCountLabel
            // 
            this.FileCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FileCountLabel.AutoSize = true;
            this.FileCountLabel.Location = new System.Drawing.Point(474, 218);
            this.FileCountLabel.Name = "FileCountLabel";
            this.FileCountLabel.Size = new System.Drawing.Size(16, 13);
            this.FileCountLabel.TabIndex = 14;
            this.FileCountLabel.Text = " 0";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Прошло времени:";
            // 
            // TimeLabel
            // 
            this.TimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Location = new System.Drawing.Point(460, 254);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(49, 13);
            this.TimeLabel.TabIndex = 16;
            this.TimeLabel.Text = "00:00:00";
            // 
            // ResultsTreeView
            // 
            this.ResultsTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResultsTreeView.Location = new System.Drawing.Point(12, 178);
            this.ResultsTreeView.Name = "ResultsTreeView";
            this.ResultsTreeView.Size = new System.Drawing.Size(339, 172);
            this.ResultsTreeView.TabIndex = 17;
            // 
            // managerBindingSource
            // 
            this.managerBindingSource.DataSource = typeof(test_ARMO.Manager);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 362);
            this.Controls.Add(this.ResultsTreeView);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FileCountLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CurrentFileLabel);
            this.Controls.Add(this.NowScanningLabel);
            this.Controls.Add(this.FoundFilesLabel);
            this.Controls.Add(this.ResumeButton);
            this.Controls.Add(this.PauseButton);
            this.Controls.Add(this.StartSearchButton);
            this.Controls.Add(this.StringToFindTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FileNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DirectoryTextBox);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(600, 350);
            this.Name = "Form1";
            this.Text = "File Search";
            ((System.ComponentModel.ISupportInitialize)(this.managerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DirectoryTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FileNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox StringToFindTextBox;
        private System.Windows.Forms.Button StartSearchButton;
        private System.Windows.Forms.Button PauseButton;
        private System.Windows.Forms.Button ResumeButton;
        private System.Windows.Forms.Label FoundFilesLabel;
        private System.Windows.Forms.Label NowScanningLabel;
        private System.Windows.Forms.Label CurrentFileLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FileCountLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.BindingSource managerBindingSource;
        private System.Windows.Forms.TreeView ResultsTreeView;
    }
}

