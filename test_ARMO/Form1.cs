﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_ARMO
{
    public partial class Form1 : Form
    {
        private Manager manager;
        public Form1()
        {
            InitializeComponent();
            manager = new Manager();            
            CurrentFileLabel.DataBindings.Add("Text", manager, "CurrentFile");
            FileCountLabel.DataBindings.Add("Text", manager, "FilesSearched");
            TimeLabel.DataBindings.Add("Text", manager, "TimeElapsed");
            DirectoryTextBox.DataBindings.Add("Text", manager, "StartingDirectory");
            FileNameTextBox.DataBindings.Add("Text", manager, "FilenamePattern");
            StringToFindTextBox.DataBindings.Add("Text", manager, "TextToFind"); 
        }

        private async void StartSearchButton_Click(object sender, EventArgs e)
        {
            manager.Results.CollectionChanged += OnResultsChanged;
            manager.StartingDirectory = DirectoryTextBox.Text;
            manager.FilenamePattern = FileNameTextBox.Text;
            manager.TextToFind = StringToFindTextBox.Text;
            ResultsTreeView.Nodes.Clear();
            ResultsTreeView.Nodes.Add(new TreeNode(" "));
            await Task.Run(() =>
            {
                try
                {
                    manager.StartSearch();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });

        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            manager.Pause();
        }

        private void ResumeButton_Click(object sender, EventArgs e)
        {
            Task.Run(() => manager.Resume());
        }

        private void EnterKeyDown (object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void OnResultsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {            
            foreach(var item in e.NewItems)
            {
                var d = new AddNodeDelegate(AddNode);
                Invoke(d, (string)item);
            }
        }

        //this is probably needlessly complicated and could be done in 2 lines?
        private void AddNode(string s)
        {
            string[] strings = s.Split(Path.DirectorySeparatorChar);
            TreeNode ExistingNode = ResultsTreeView.Nodes[0];
            int lastIndex = 0;
            for(int i = 0; i != strings.Length; i++)
            {
                var found = ResultsTreeView.Nodes.Find(strings[i], true);
                if (found.Length == 0) break;
                else
                {
                    ExistingNode = found.Last();
                    lastIndex++;
                }
            }            
            for(int i = lastIndex; i != strings.Length; i++)
            {
                TreeNode newNode = new TreeNode(strings[i]);
                newNode.Name = strings[i];
                ExistingNode.Nodes.Add(newNode);
                ExistingNode = newNode;
                ResultsTreeView.ExpandAll();
            }
        }

        public delegate void AddNodeDelegate(string s);
    }
}
